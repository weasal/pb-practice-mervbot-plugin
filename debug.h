#pragma once

#ifdef _DEBUG
#include <iostream>
#include <ctime>

inline std::string getTimestamp() {
	time_t rawtime;
	time(&rawtime);

	struct tm timeinfo;
	localtime_s(&timeinfo, &rawtime);

	char buffer[80];
	strftime(buffer, 80, "%I:%M:%S", &timeinfo);
	return buffer;
}
#endif

inline void debugOut(std::string format, ...)
{
#ifdef _DEBUG
	va_list args;
	va_start(args, format);
	std::cout << "[DEBUG " << getTimestamp() << "] ";
	vprintf(format.c_str(), args);
	std::cout << std::endl;
	va_end(args);
#endif
}
