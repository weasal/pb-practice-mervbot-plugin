#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <unordered_map>
#include <random>
#include <algorithm>
#include <chrono>
#include <memory>
#include <functional>

// Powerball related includes
#include "powerballgame\powerballgame.h"
#include "powerballsite\powerballsite.h"
#include "powerballsite\gamereport.h"

#define STRING_CAST_CHAR
#include "dllcore.h"
#include "clientprot.h"

#include "timeout.h"
#include "debug.h"

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif
