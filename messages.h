// Preprocessor, make string literals out of int literals
#define STRINGIFY(x)	#x
#define TOSTRING(x)		STRINGIFY(x)

// These might depend on server (subgame2 or asss)
#define CMD_REMOVEBALL			"?set Soccer:BallCount:0"
#define CMD_ADDBALL				"?set Soccer:BallCount:1"
// shipNumber must be an integer literal
#define CMD_SETSHIP(shipNumber)	"*setship " TOSTRING(shipNumber)
// freqNumber must be an integer literal
#define CMD_SETFREQ(freqNumber) "*setfreq " TOSTRING(freqNumber)
// x and y must be integer literals
#define CMD_MOVE_TO(x, y)		"*warpto " TOSTRING(x) " " TOSTRING(y)

#define CMD_LOCK				"*lock"
#define CMD_SPEC				"*spec"
#define CMD_SPECALL				"*specall"

#define CMD_WARP				"*prize #7"
#define CMD_ENGINES_SHUTDOWN	"*prize #14"

#define CMD_ARENA_MSG			"*arena "
#define ARENA_MESSAGE(x)		CMD_ARENA_MSG x

#define MSG_PLUGIN_INFO			"[pbpractice.dll] Powerball practice plugin by Crescendo <weasalss@gmail.com>"

#define PB_SITE_URL				"http://powerball.svssubspace.com"
#define PB_URI_HELP				"/league/help"
#define PB_URI_SIGNUP			"/signup"
#define PB_URI_GAMES			"/games"
#define MSG_BOT_HELP			"See " PB_SITE_URL PB_URI_HELP " for more information and help on how to use it."
#define MSG_ABOUT_BOT			"This is a powerball practice arena bot."

// Messages used to interact with players and for powerball games
#define MSG_BALANCING_TEAMS		ARENA_MESSAGE("Teams balanced.")
#define MSG_GAME_STARTING(x)	ARENA_MESSAGE("Game starting in the next " TOSTRING(x) " seconds...")
#define MSG_GAME_START			ARENA_MESSAGE("Go!")
#define MSG_GAME_ENDNOSTART		ARENA_MESSAGE("Game ended before starting.")
#define MSG_GAME_OVERTIME		ARENA_MESSAGE("Overtime. Next goal wins.")
#define MSG_GAME_END			ARENA_MESSAGE("Game over.")
#define MSG_MERCY_RULE_END		ARENA_MESSAGE("Game ended due to mercy rule.")

/* Teams related */
// Received when a player attempts to queue when he is already in staging
#define MSG_INSTAGING			"You are already allowed to play. Use !sub to enter."
// Received when a player attempts to enter out of turn
#define MSG_CANNOTENTER			"Please wait for your turn to play. Use !next to get in line if you haven't already."
// Received when it is a player's turn to enter
#define MSG_CANENTER			"You are next to play and a spot is available. You have " TOSTRING(STAGING_TIME_LIMIT) " seconds to enter using !sub or you will lose your spot."
// Received when a player was next in line but did not enter in the alloted period
#define MSG_LOSTENTRY			"You lost your spot."
// Received when a player is kicked because their playing will cause uneven teams
#define MSG_UNEVENKICK			"You were removed from play due to uneven teams. You are next in line to play."

// MSG_ADDEDTOQUEUE doesn't exist because it is dynamically generated (see teamsmanager.cpp)
#define MSG_REMOVEDFROMQUEUE	"Removed from the queue if you were in it."
#define MSG_ALREADYQUEUED		"You are already in line to play."
#define MSG_CANNOTQUEUE			"Not added to the queue (you may already be playing or the queue is full)"

/* Site related */
#define MSG_REGISTER			"Please register at " PB_SITE_URL PB_URI_SIGNUP " to play."
#define MSG_CONFIRMSUCCESS		"Confirmed! Please return to the site to finish creating your account."
#define MSG_CONFIRMFAILURE		"Confirmation error. Please check that your code is correct and nick matches what you signed up with."
#define MSG_GAMENOTSAVED		ARENA_MESSAGE("Unable to save last game (an error occured)")

/* Score related */
#define MSG_TIED				"Tie"
#define MSG_WBSWINNING			"Warbirds"
#define MSG_JAVSWINNING			"Javelins"			