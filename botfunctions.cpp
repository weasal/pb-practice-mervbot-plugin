#include "stdafx.h"
#include "spawn.h"
#include "settings.h"
#include "messages.h"
#include "sounds.h"
#include "lvz.h"

// Called when a player uses Esc+Q or or Esc+S
void botInfo::playerSpecOrQuit(const Player* p, int oldShip) {
	if (teamsManager.isActivePlayer(p)) { // Player was in play
		debugOut("Player exited active game: %s", getPlayerName(p).c_str());
		teamsManager.handlePlayerExit(p, oldShip);

		if (currentGame) {
			std::string name = getPlayerName(p);
			currentGame->logPilotExit(name);

			if (teamsManager.getActivePlayers().empty()) {
				// Premature end
				// Anti-griefing: if the winning team is the forfeiting team, make sure the reported score
				// is not in favor of the forfeiting team
				std::stringstream forfeitMessage; // (e.g. Game forfeited by the Javelins. Reporting score as 7-6 in favor of the Warbirds.)
				forfeitMessage << "*arena Game forfeited by the ";

				Powerball::Team lastPlayingTeam = SHIP_TO_POWERBALLTEAM(oldShip);
				Powerball::Team forfeitedTeam = (lastPlayingTeam == Powerball::Team::Warbirds) ?
					Powerball::Team::Javelins : Powerball::Team::Warbirds;

				forfeitMessage << (forfeitedTeam == Powerball::Team::Warbirds ? "Warbirds. " : "Javelins. ");

				auto score = currentGame->getScore();
				if (score[forfeitedTeam] >= score[lastPlayingTeam]) {
					score[lastPlayingTeam] = score[forfeitedTeam] + 1;

					forfeitMessage << "Reporting score as " << score[Powerball::Team::Warbirds] << "-" <<
						score[Powerball::Team::Javelins] << " in favor of the ";
					forfeitMessage << (lastPlayingTeam == Powerball::Team::Warbirds ? "Warbirds." : "Javelins.");
				}

				sendPublic(const_cast<char*>(forfeitMessage.str().c_str()));
				endPowerballGame(score);
			}
		}

		if (botState != BotState::NoGame && botState != BotState::Intermission && botState != BotState::BalancingTeams)
			teamsManager.stageQueuedPlayers();
	}

	playerSwitches.remove(p);
}

void botInfo::balanceTeams() {
	debugOut("Balancing teams");
	playerSwitches.clear();
	sendPublic(MSG_BALANCING_TEAMS);

	botState = BotState::BalancingTeams;

	auto activePlayers = teamsManager.getActivePlayers();
	if (activePlayers.size() > 2) {
		std::vector<std::string> playerNames;
		for (const Player* p : teamsManager.getActivePlayers())
			playerNames.push_back(getPlayerName(p));

		// Returns an array containing two string vectors, each vector representing a team of players
		std::array<std::vector<std::string>, 2> teams = site.getBalancedTeams(playerNames);
		auto warbirds = teams[0]; // doesn't matter which one is used for warbirds or javs
		auto javelins = teams[1];

		for (const Player* p : teamsManager.getActivePlayers()) {
			std::string name = getPlayerName(p);

			// Make sure that each player is on the corresponding balanced team, if missing, then switch them
			// to the other team
			if (p->ship == SHIP_Warbird && std::find(warbirds.begin(), warbirds.end(), name) == warbirds.end()) {
				sendPrivate(p, CMD_SETSHIP(2));
				sendPrivate(p, CMD_SETFREQ(FREQ_JAVELINS));

				debugOut("Changing %s's team to javelins", getPlayerName(p).c_str());
				playerSwitches.push_back(p);
			}
			else if (p->ship == SHIP_Javelin && std::find(javelins.begin(), javelins.end(), name) == javelins.end()) {
				sendPrivate(p, CMD_SETSHIP(1));
				sendPrivate(p, CMD_SETFREQ(FREQ_WARBIRDS));

				debugOut("Changing %s's team to warbirds", getPlayerName(p).c_str());
				playerSwitches.push_back(p);
			}
		}

#ifdef _DEBUG
		std::stringstream balanceLog;
		bool firstEntry = true;
		balanceLog << "Waiting on EVENT_PlayerTeam for [";
		for (const Player* switchedPlayer : playerSwitches) {
			if (!firstEntry)
				balanceLog << ", ";

			balanceLog << getPlayerName(switchedPlayer);
			firstEntry = false;
		}
		balanceLog << "]";
		debugOut("%s", balanceLog.str().c_str());
#endif
	}
}

void botInfo::rebuildPlayerMap() {
	playerMap.clear();

	/* Initialize the playerMap */
	_listnode<Player>* parse = playerlist->head;
	while (parse) {
		Player* p = parse->item;
		playerMap[p->ident] = p;
		parse = parse->next;
	}
}

#define START_DELAY 13
#define READY_PERIOD 2
#define SET_PERIOD_MIN 2
#define SET_PERIOD_MAX 5
#define TIME_TILL_GO 20 // Just the sum of START_DELAY + READY_PERIOD + SET_PERIOD_MAX
void botInfo::startPowerballGame() {
	// Create a game
	botState = BotState::StartingGame;
	currentGame = std::make_shared<Powerball::Game>(std::chrono::minutes(DEFAULT_GAME_LENGTH));
	setGamescoreLvz(currentGame->getScore());
	setLeadLvz(currentGame->getLength(), currentGame->getScore());

	// Add all players currently in ships to the newly created game
	for (const Player* activePlayer : teamsManager.getActivePlayers()) {
		currentGame->logPilotEntry(getPlayerName(activePlayer), SHIP_TO_POWERBALLTEAM(activePlayer->ship));
	}

	sendPublic(SOUND_NOTIFY, MSG_GAME_STARTING(TIME_TILL_GO));

	// After the start delay, display the "READY" lvz graphics and play the ready sound
	Timeout::set(START_DELAY, [this]() {
		if (currentGame && currentGame->getState() == Powerball::Game::State::NotStarted) {
			queue_enable(LVZ_READY);
			toggle_objects();

			sendPublic(SOUND_READY, "*arena");
		}
	});

	// After the ready delay, display the "SET" lvz graphics and play the set sound
	Timeout::set(START_DELAY + READY_PERIOD, [this]() {
		if (currentGame && currentGame->getState() == Powerball::Game::State::NotStarted) {
			queue_disable(LVZ_READY);
			queue_enable(LVZ_SET);
			toggle_objects();

			sendPublic(SOUND_SET, "*arena");
		}
	});

	// Make a copy of the currentGame shared_ptr by using std::bind
	std::function<void(void)> ender = std::bind([this](std::shared_ptr<Powerball::Game> copiedGame) {
		// There's a possibility that it could've ended prematurely, and a new game might've started
		// So check that it hasn't and is the current game before ending it
		if (currentGame && copiedGame == currentGame) {
			std::unordered_map<Powerball::Team, int> score = currentGame->getScore();

			bool tied = (score[Powerball::Team::Warbirds] == score[Powerball::Team::Javelins]);
			if (tied && (botState != BotState::GameOvertime) && OVERTIME_LENGTH > 0) {
				botState = BotState::GameOvertime;
				sendPublic(SOUND_BELL, MSG_GAME_OVERTIME);

				Timeout::set(60 * OVERTIME_LENGTH, [this, copiedGame]() {
					if (currentGame && copiedGame == currentGame) {
						endPowerballGame();
					}
				});
			}
			else {
				endPowerballGame();
			}
		}
	}, currentGame);

	// Display the "GO" lvz and play the go sound
	std::default_random_engine e;
	std::uniform_int_distribution<> distribution{SET_PERIOD_MIN, SET_PERIOD_MAX};
	int randomStart = distribution(e) + START_DELAY + READY_PERIOD;
	Timeout::set(randomStart, [this, ender]() {
		if (currentGame && currentGame->getState() == Powerball::Game::State::NotStarted) {
			botState = BotState::InGame;

			queue_disable(LVZ_SET);
			queue_enable(LVZ_GO);
			toggle_objects();

			sendPublic(SOUND_GO, MSG_GAME_START);
			int gameLength = static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(currentGame->getLength()).count());

			Timeout::set(gameLength, ender);
			currentGame->start();
		}
	});
}

GameReport makeGameReport(Powerball::Game game) {
	GameReport report = {};
	report.matchId = 0;

	auto score = game.getScore();
	report.startedAt = game.getTimeStarted();
	report.score.warbirds = score[Powerball::Team::Warbirds];
	report.score.javelins = score[Powerball::Team::Javelins];
	report.length = game.getLength().count() * 60; // in seconds

	report.goals = std::vector<LoggedGoal>();
	for (auto goal : game.getGoals()) {
		LoggedGoal g = {};
		g.team = (goal.team == Powerball::Team::Warbirds) ? "warbirds" : "javelins";
		g.scorer = goal.scorer;
		g.assister = goal.assister;
		g.heldFor = goal.carriedFor;
		g.shotAt = goal.shotAt;
		g.scoredAt = goal.scoredAt;
		g.scoredOn = std::vector<std::string>();
		for (std::string goalie : goal.goalies) {
			g.scoredOn.push_back(goalie);
		}
		for (auto point : goal.trajectory) {
			g.trajectory.push_back(std::pair<double, double>(point.x(), point.y()));
		}
		report.goals.push_back(g);
	}

	report.participants = std::vector<LoggedParticipant>();
	for (auto participant : game.getParticipants()) {
		LoggedParticipant p = {};
		p.name = participant.getName();
		p.team = (participant.getTeam() == Powerball::Team::Warbirds) ? "warbirds" : "javelins";

		p.activeTimes = std::vector < std::pair<std::chrono::time_point<std::chrono::system_clock>,
			std::chrono::time_point<std::chrono::system_clock >> >();
		for (auto interval : participant.getActiveTimes()) {
			p.activeTimes.push_back(interval);
		}

		p.playtime = static_cast<int>(participant.getPlaytime().count());

		p.stats = {};
		p.stats.assists = participant.getStat(Powerball::Participant::Stat::Assist);
		p.stats.ballcarries = participant.getStat(Powerball::Participant::Stat::BallCarry);
		p.stats.deaths = participant.getStat(Powerball::Participant::Stat::Death);
		p.stats.goals = participant.getStat(Powerball::Participant::Stat::Goal);
		p.stats.goalsallowed = participant.getStat(Powerball::Participant::Stat::GoalAllowed);
		p.stats.kills = participant.getStat(Powerball::Participant::Stat::Kill);
		p.stats.pressures = participant.getStat(Powerball::Participant::Stat::Pressure);
		p.stats.saves = participant.getStat(Powerball::Participant::Stat::Save);
		p.stats.steals = participant.getStat(Powerball::Participant::Stat::Steal);
		p.stats.turnovers = participant.getStat(Powerball::Participant::Stat::Turnover);

		report.participants.push_back(p);
	}

	return report;
}

#define MIN_REPORT_LENGTH std::chrono::seconds(30)
void botInfo::endPowerballGame(std::unordered_map<Powerball::Team, int> scoreOverride) {
	if (currentGame && currentGame->getState() == Powerball::Game::State::InProgress) {
		std::unordered_map<Powerball::Team, int> score;
		if (scoreOverride.empty())
			score = currentGame->getScore();
		else
			score = scoreOverride;

		std::chrono::seconds halfGameLength(currentGame->getLength() / 2);

		if (score[Powerball::Team::Warbirds] != score[Powerball::Team::Javelins]) {
			Powerball::Team winner = (score[Powerball::Team::Warbirds] > score[Powerball::Team::Javelins]) ?
				Powerball::Team::Warbirds : Powerball::Team::Javelins;

			Timeout::set(3, [this, winner]() {
				if (winner == Powerball::Team::Warbirds) {
					sendPublic(SOUND_WARBIRDS_WIN, CMD_ARENA_MSG);
					queue_enable(LVZ_WARBIRDS_WIN);
				}
				else {
					sendPublic(SOUND_JAVELINS_WIN, CMD_ARENA_MSG);
					queue_enable(LVZ_JAVELINS_WIN);
				}

				toggle_objects();
			});

			// Spec the losing team's players who played for more than half the game on the losing team
			int losingTeamShip = (winner == Powerball::Team::Warbirds) ? SHIP_Javelin : SHIP_Warbird;
			for (auto activePlayer : teamsManager.getActivePlayers()) {
				bool loser = (activePlayer->ship == losingTeamShip);
				if (loser) {
					if (currentGame->getPlayerPlaytime(getPlayerName(activePlayer), SHIP_TO_POWERBALLTEAM(activePlayer->ship)) > halfGameLength) {
						sendPrivate(activePlayer, CMD_SPEC);
						sendPrivate(activePlayer, CMD_SPEC); // Unlock
					}
				}
			}
		} 
		else {
			// Eject all the players (who played for more than half the game)
			for (auto activePlayer : teamsManager.getActivePlayers()) {
				if (currentGame->getPlayerPlaytime(getPlayerName(activePlayer), SHIP_TO_POWERBALLTEAM(activePlayer->ship)) > halfGameLength) {
					sendPrivate(activePlayer, CMD_SPEC);
					sendPrivate(activePlayer, CMD_SPEC); // Unlock
				}
			}
		}

		sendPublic(SOUND_GAMEOVER, MSG_GAME_END);
		queue_enable(LVZ_GAME_OVER);
		toggle_objects();

		currentGame->end();

		// Print the game stats
		std::list<std::string> gameResults = currentGame->getResultsOutput();
		for (std::string line : gameResults) {
			std::string arenaMessage = "*arena ";
			arenaMessage.append(line);
			sendPublic(const_cast<char*>(arenaMessage.c_str()));
		}

		// Delay sending the game report until later so the bot's fast (can immediately toggle lvlz and stuff)
		// rather than waiting hundreds of milliseconds for the http request to execute and finish.
		std::shared_ptr<Powerball::Game> unreportedGame = currentGame;
		if (currentGame->getLength() > MIN_REPORT_LENGTH) {
			Timeout::set(INTERMISSION_LENGTH / 4, [this, unreportedGame, score]() {
				// report the game to the powerball site
				GameReport report = {}; // makeGameReport(unreportedGame);
				report.score.warbirds = score.at(Powerball::Team::Warbirds);
				report.score.javelins = score.at(Powerball::Team::Javelins);

				int gameIndex = site.reportGame(report, MAP_NAME, false);

				if (gameIndex >= 0) {
					std::stringstream gameReportMessage;
					gameReportMessage << CMD_ARENA_MSG "Game saved: " PB_SITE_URL PB_URI_GAMES "/" << gameIndex;
					sendPublic(const_cast<char*>(gameReportMessage.str().c_str()));
				}
				else {
					sendPublic(MSG_GAMENOTSAVED);
				}
			});
		}
	} else {
		sendPublic(MSG_GAME_ENDNOSTART);
	}

	currentGame.reset();
	botState = BotState::NoGame;
}

void botInfo::setGamescoreLvz(std::unordered_map<Powerball::Team, int> score, Player* specificPlayer) {
	if (specificPlayer)
		object_target(specificPlayer);

	int oldJavs = lvzScore[Powerball::Team::Javelins];
	int oldJavsTens = (oldJavs / 10) % 10; // Add the modulo in case of scores exceeding 99
	int oldJavsOnes = oldJavs % 10;

	int newJavs = score[Powerball::Team::Javelins];
	int newJavsTens = (newJavs / 10) % 10;
	int newJavsOnes = newJavs % 10;

	int oldBirds = lvzScore[Powerball::Team::Warbirds];
	int oldBirdsTens = (oldBirds / 10) % 10;
	int oldBirdsOnes = oldBirds % 10;

	int newBirds = score[Powerball::Team::Warbirds];
	int newBirdsTens = (newBirds / 10) % 10;
	int newBirdsOnes = newBirds % 10;

	lvzScore = score;

	if (oldJavsTens != newJavsTens) {
		queue_disable(LVZ_JAVELIN_SCORE_TENS(oldJavsTens));
	}
	queue_enable(LVZ_JAVELIN_SCORE_TENS(newJavsTens));

	if (oldJavsOnes != newJavsOnes) {
		queue_disable(LVZ_JAVELIN_SCORE_ONES(oldJavsOnes));
	}
	queue_enable(LVZ_JAVELIN_SCORE_ONES(newJavsOnes));

	if (oldBirdsTens != newBirdsTens) {
		queue_disable(LVZ_WARBIRD_SCORE_TENS(oldBirdsTens));
	}
	queue_enable(LVZ_WARBIRD_SCORE_TENS(newBirdsTens));

	if (oldBirdsOnes != newBirdsOnes) {
		queue_disable(LVZ_WARBIRD_SCORE_ONES(oldBirdsOnes));
	}
	queue_enable(LVZ_WARBIRD_SCORE_ONES(newBirdsOnes));

	toggle_objects();
	object_target(nullptr);
}

void botInfo::setLeadLvz(std::chrono::seconds remainingTime, std::unordered_map<Powerball::Team, int> score, Player* specificPlayer) {
	if (specificPlayer)
		object_target(specificPlayer);
	
	bool isGamePoint = false; // unused in timed games
	bool tied = (score[Powerball::Team::Warbirds] == score[Powerball::Team::Javelins]);

	if (tied) {
		queue_disable(LVZ_LEAD_WARBIRDS);
		queue_disable(LVZ_GAMEPOINT_WARBIRDS);
		queue_disable(LVZ_LEAD_JAVELINS);
		queue_disable(LVZ_GAMEPOINT_JAVELINS);
	}
	else {
		if (isGamePoint || remainingTime <= std::chrono::seconds(DIRE_LEAD_TIME)) {
			if (score[Powerball::Team::Warbirds] > score[Powerball::Team::Javelins]) {
				queue_disable(LVZ_LEAD_WARBIRDS);
				queue_enable(LVZ_GAMEPOINT_WARBIRDS);
				queue_disable(LVZ_LEAD_JAVELINS);
				queue_disable(LVZ_GAMEPOINT_JAVELINS);
			}
			else {
				queue_disable(LVZ_LEAD_WARBIRDS);
				queue_disable(LVZ_GAMEPOINT_WARBIRDS);
				queue_disable(LVZ_LEAD_JAVELINS);
				queue_enable(LVZ_GAMEPOINT_JAVELINS);
			}
		}
		else {
			if (score[Powerball::Team::Warbirds] > score[Powerball::Team::Javelins]) {
				queue_enable(LVZ_LEAD_WARBIRDS);
				queue_disable(LVZ_GAMEPOINT_WARBIRDS);
				queue_disable(LVZ_LEAD_JAVELINS);
				queue_disable(LVZ_GAMEPOINT_JAVELINS);
			}
			else {
				queue_disable(LVZ_LEAD_WARBIRDS);
				queue_disable(LVZ_GAMEPOINT_WARBIRDS);
				queue_enable(LVZ_LEAD_JAVELINS);
				queue_disable(LVZ_LEAD_WARBIRDS);
			}
		}
	}

	toggle_objects();
	object_target(nullptr);
}

void botInfo::setGametimeLvz(std::chrono::seconds secondsPassed) {
	int oldCount = static_cast<int>(lvzSeconds.count());
	int oldMinutes = oldCount / 60;
	int oldMinuteTens = (oldMinutes / 10) % 10; // div by 10 minutes
	int oldMinuteOnes = oldMinutes % 10; // mod 10 min

	int oldSeconds = (oldCount % 60);
	int oldSecondTens = (oldSeconds / 10);
	int oldSecondOnes = oldSeconds % 10;

	int newCount = static_cast<int>(secondsPassed.count());
	int newMinutes = newCount / 60;
	int newMinuteTens = (newMinutes / 10) % 10; // div by 10 minutes
	int newMinuteOnes = newMinutes % 10; // mod 10 min

	int newSeconds = (newCount % 60);
	int newSecondTens = (newSeconds / 10);
	int newSecondOnes = newSeconds % 10;

	lvzSeconds = secondsPassed;

	if (oldMinuteTens != newMinuteTens) {
		queue_disable(LVZ_CLOCK_MIN_TENS(oldMinuteTens));
	}
	queue_enable(LVZ_CLOCK_MIN_TENS(newMinuteTens));

	if (oldMinuteOnes != newMinuteOnes) {
		queue_disable(LVZ_CLOCK_MIN_ONES(oldMinuteOnes));
	}
	queue_enable(LVZ_CLOCK_MIN_ONES(newMinuteOnes));

	if (oldSecondTens != newSecondTens) {
		queue_disable(LVZ_CLOCK_SEC_TENS(oldSecondTens));
	}
	queue_enable(LVZ_CLOCK_SEC_TENS(newSecondTens));

	if (oldSecondOnes != newSecondOnes) {
		queue_disable(LVZ_CLOCK_SEC_ONES(oldSecondOnes));
	}
	queue_enable(LVZ_CLOCK_SEC_ONES(newSecondOnes));

	toggle_objects();
}