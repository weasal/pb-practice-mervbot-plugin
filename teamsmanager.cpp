#include "stdafx.h"
#include "spawn.h"
#include "messages.h"

TeamsManager::TeamsManager(botInfo* bot, int maxTeamSize) {
	this->bot = bot;
	this->maxTeamSize = maxTeamSize;
	std::unordered_map<int, int> teamSize({{ SHIP_Warbird, 0 }, { SHIP_Javelin, 0 }});
}

bool TeamsManager::isActivePlayer(const Player* p) {
	return std::find(activePlayers.begin(), activePlayers.end(), p) != activePlayers.end();
}

bool TeamsManager::isStagingPlayer(std::string name) {
	return std::find(stagingPlayers.begin(), stagingPlayers.end(), name) != stagingPlayers.end();
}

int TeamsManager::getTeamSize(int ship) {
	if (ship == SHIP_Warbird || ship == SHIP_Javelin)
		return teamSize[ship];

	return 0;
}

int TeamsManager::getMaxTeamSize() {
	return maxTeamSize;
}

std::list<const Player*> TeamsManager::getActivePlayers() {
	return activePlayers;
}

bool TeamsManager::teamsAreFull() {
	return activePlayers.size() >= static_cast<unsigned int>(2 * maxTeamSize);
}

std::list<std::string> TeamsManager::getQueue() {
	return playerQueue;
}

std::string composeQueueResponse(int position) {
	std::stringstream response;
	response << "You are ";

	switch (position) {
	case 1:
		response << "next";
		break;
	case 2:
		response << "2nd";
		break;
	case 3:
		response << "3rd";
		break;
	default:
		response << position << "th";
		break;
	}

	response << " in line to play.";
	return response.str();
}

// !next command
bool TeamsManager::queuePlayer(const Player *p) {
	std::string name = bot->getPlayerName(p);

	if (isStagingPlayer(name)) {
		bot->sendPrivate(p, MSG_INSTAGING);
		return false;
	}

	int size = playerQueue.size();
	if (size >= QUEUE_LIMIT || isActivePlayer(p)) {
		bot->sendPrivate(p, MSG_CANNOTQUEUE);
		return false;
	}

	for (auto iter = playerQueue.begin(); iter != playerQueue.end(); iter++) {
		if (*iter == name) {
			bot->sendPrivate(p, MSG_ALREADYQUEUED);
			return false;
		}
	}

	playerQueue.push_back(name);

	// Send player "You are nth in line to play."
	bot->sendPrivate(p, const_cast<char*>(composeQueueResponse(playerQueue.size()).c_str()) );

	return true;
}

// !remove command
void TeamsManager::unqueuePlayer(const Player *p) {
	playerQueue.remove(bot->getPlayerName(p));
	bot->sendPrivate(p, MSG_REMOVEDFROMQUEUE);
}

void TeamsManager::stageQueuedPlayers() {
	std::vector<std::string> newlyStagedPlayers;

	while (canDequeueToStaging()) {
		std::string nextPlayer = playerQueue.front();
		playerQueue.pop_front();
		stagingPlayers.push_back(nextPlayer);

		newlyStagedPlayers.push_back(nextPlayer);
	}

	if (!newlyStagedPlayers.empty()) {
		for (auto name : newlyStagedPlayers) {
			bot->sendRemotePrivate(const_cast<char*>(name.c_str()), MSG_CANENTER);
		}

		Timeout::set(STAGING_TIME_LIMIT, [this, newlyStagedPlayers]() {
			// isStagingPlayer is a private method in TeamsManager, however:
			// See http://stackoverflow.com/questions/12731215/access-rights-of-a-lambda-capturing-this
			// "A local class of a member function may access the same names that the member function itself may access"
			for (auto stagedName : newlyStagedPlayers) {
				if (this->isStagingPlayer(stagedName)) { // Check if the player is still in staging (i.e. did not enter).
					this->removeFromStaging(stagedName);
					this->bot->sendRemotePrivate(const_cast<char*>(stagedName.c_str()), MSG_LOSTENTRY);
				}
			}

			this->stageQueuedPlayers();
		});
	}
	else if (stagingPlayers.empty()) {
		// If no staged and no newly staged players make sure teams are even
		ensureEvenTeams();
	}
}

void TeamsManager::removeFromStaging(std::string name) {
	stagingPlayers.remove(name);
	return;
}

bool TeamsManager::hasStagingPlayers() {
	return !stagingPlayers.empty();
}

#define IS_ODD(x) (((x) % 2) == 1)
bool TeamsManager::canDequeueToStaging() {
	if (playerQueue.size() > 0) {
		int potentialSize = activePlayers.size() + stagingPlayers.size();
		bool mightBeFull = (potentialSize >= 2 * maxTeamSize); // Make sure the game won't potentially be full

		// Make sure it won't be stuck odd (there is only 1 player in the queue to be added, and adding them
		// will result in a odd number of players)
		bool stuckOdd = (playerQueue.size() == 1) && IS_ODD(potentialSize + 1);
		return !mightBeFull && !stuckOdd;
	}

	return false;
}

/* Returns true if player entry is possible, false otherwise */
bool TeamsManager::playerCanEnter(const Player* p) {
	return isStagingPlayer(bot->getPlayerName(p));
}

// Called when a player who was in spec enters the game
// Note: the arena is *lock'd, so players may not enter the game freely nor
// can they change their team, so this is only called as a result of a !sub command
void TeamsManager::handlePlayerEntry(const Player* p, int ship) {
	std::string name = bot->getPlayerName(p);

	activePlayers.push_back(p);
	teamSize[ship]++;

	if (isStagingPlayer(name))
		removeFromStaging(name);

	if (!hasStagingPlayers())
		ensureEvenTeams();
}

void TeamsManager::handleTeamSwitch(const Player* p, int ship) {
	debugOut("(handleTeamSwitch) Player switched teams: %s", bot->getPlayerName(p).c_str());
	int oldTeamShip = (ship == SHIP_Warbird) ? SHIP_Javelin : SHIP_Warbird;
	teamSize[oldTeamShip]--;
	teamSize[ship]++;
}

// Called when a player who was in a ship exits the game
void TeamsManager::handlePlayerExit(const Player* p, int oldShip) {
	activePlayers.remove(p);
	teamSize[oldShip]--;
}

void TeamsManager::ensureEvenTeams() {
	// Make sure that teams are even

#ifdef _DEBUG
	int warbirdsActual = 0, javelinsActual = 0;

	for (const Player* player : activePlayers) {
		if (player->ship == SHIP_Warbird) {
			warbirdsActual++;
		}
		else if (player->ship == SHIP_Javelin) {
			javelinsActual++;
		}
	}

	if (warbirdsActual != teamSize[SHIP_Warbird]) {
		debugOut("SIZE MISMATCH! Warbirds %i %i", warbirdsActual, teamSize[SHIP_Warbird]);
		throw std::runtime_error("Team size inaccuracy");
	} 
	else if (javelinsActual != teamSize[SHIP_Javelin]) {
		debugOut("SIZE MISMATCH! Javelins %i %i", javelinsActual, teamSize[SHIP_Javelin]);
		throw std::runtime_error("Team size inaccuracy");
	}
#endif

	if (teamSize[SHIP_Warbird] != teamSize[SHIP_Javelin]) {
		std::list<const Player*> removedPlayers;

		int difference = abs(teamSize[SHIP_Warbird] - teamSize[SHIP_Javelin]);
		int removeTeam = (teamSize[SHIP_Warbird] > teamSize[SHIP_Javelin]) ? SHIP_Warbird : SHIP_Javelin;
		for (auto iter = activePlayers.rbegin(); iter != activePlayers.rend(); iter++) {
			if (difference == 0)
				break;

			if ((*iter)->ship == removeTeam) {
				removedPlayers.push_front(*iter);
				difference--;
			}
		}

		for (auto removedPlayer : removedPlayers) {
			debugOut("Player removed from the game to even teams: %s", bot->getPlayerName(removedPlayer).c_str());
			bot->sendPrivate(removedPlayer, CMD_SPEC);
			bot->sendPrivate(removedPlayer, CMD_SPEC); // Unlock

			playerQueue.push_front(bot->getPlayerName(removedPlayer));
			bot->sendPrivate(removedPlayer, MSG_UNEVENKICK);
		}
	}
}