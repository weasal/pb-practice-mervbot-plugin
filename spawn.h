/*
	MERVBot Spawn-callback by Catid@pacbell.net
*/

#ifndef SPAWN_H
#define SPAWN_H

#include "settings.h"
#include "teamsmanager.h"
#include "goalarea.h"

#define REGISTER_CACHE_MAX 100
#define MAX_OBJECTS 20

#define SHIP_TO_POWERBALLTEAM(x) ((x == SHIP_Warbird) ? Powerball::Team::Warbirds : Powerball::Team::Javelins)
class botInfo
{
	bool CONNECTION_DENIED;

	CALL_HANDLE handle;
	CALL_COMMAND callback;
	CALL_PLIST playerlist;
	CALL_FLIST flaglist;
	CALL_MAP map;
	CALL_BLIST bricklist;
	char *arena;
	arenaSettings *settings;
	Player *me;
	bool biller_online;

	objectInfo object_array[MAX_OBJECTS];
	int num_objects;
	Player *object_dest;

	// Powerball bot specific members -----------------------------------------------
	/**
	Initial bot state is NoGame.
	NoGame (on bot's arena enter) -> Intermission.
	Intermission (after INTERMISSION_LENGTH) -> PlayerEntry,
	PlayerEntry (when all players who can enter, have entered) -> BalancingTeams,
	BalancingTeams (when teams have been balanced) -> SpawningBall
	SpawningBall (when ball has spawned) -> StartingGame
	StartingGame (when game has started) -> InGame
	-- InGame (if score is tied after DEFAULT_GAME_LENGTH) -> GameOvertime
	-- GameOvertime (after OVERTIME_LENGTH) -> Intermission
	InGame (after DEFAULT_GAME_LENGTH and not tied) -> Intermission
	*/
	static enum class BotState { NoGame, Intermission, PlayerEntry, BalancingTeams, SpawningBall, StartingGame, InGame, GameOvertime };
	BotState botState;
	bool arenaHasBall;

	std::unordered_map<int, Player*> playerMap; // playerMap rebuilt on init and arena entry
	void rebuildPlayerMap();

	std::list<std::string> registerCheckCache;

	PowerballSite site;
	TeamsManager teamsManager;
	std::shared_ptr<Powerball::Game> currentGame;

	GoalArea warbirdsGoal;
	GoalArea javelinsGoal;

	void playerSpecOrQuit(const Player* p, int oldShip); // Only called in EVENT_PlayerSpec and EVENT_PlayerLeaving

	void startPowerballGame();
	void endPowerballGame(std::unordered_map<Powerball::Team, int> scoreOverride = {});

	std::chrono::seconds lvzSeconds; // used for clearing the previously set time
	void setGametimeLvz(std::chrono::seconds seconds);
	std::unordered_map<Powerball::Team, int> lvzScore;
	// if specificPlayer is not null, then this will only update the graphics for that specific player (only on his client)
	void setGamescoreLvz(std::unordered_map<Powerball::Team, int> score, Player* specificPlayer = nullptr);
	void setLeadLvz(std::chrono::seconds remainingTime, std::unordered_map<Powerball::Team, int> score, Player* specificPlayer = nullptr);

	// Some ball information needed for lvz information
	Player* previousCarrier;

	std::list<const Player*> playerSwitches;
	void botInfo::balanceTeams();
public:
	inline std::string getPlayerName(const Player* p) {
		return std::string(p->name);
	}

	botInfo(CALL_HANDLE given) : site(), teamsManager(this, DEFAULT_TEAMSIZE), currentGame(nullptr),
		warbirdsGoal(Powerball::Team::Warbirds, WARBIRDS_GOAL_LEFT, WARBIRDS_GOAL_TOP, WARBIRDS_GOAL_RIGHT, WARBIRDS_GOAL_BOTTOM),
		javelinsGoal(Powerball::Team::Javelins, JAVELINS_GOAL_LEFT, JAVELINS_GOAL_TOP, JAVELINS_GOAL_RIGHT, JAVELINS_GOAL_BOTTOM)
	{
		handle = given;
		callback = 0;
		playerlist = 0;
		flaglist = 0;
		map = 0;
		CONNECTION_DENIED = false;
		me = 0;
		biller_online = true;
		num_objects = 0;
		object_dest = NULL;

		botState = BotState::NoGame;

		lvzSeconds = std::chrono::seconds(0);
		lvzScore = std::unordered_map<Powerball::Team, int>({
				{ Powerball::Team::Warbirds, 0 },
				{ Powerball::Team::Javelins, 0 }
		});
	}

	void clear_objects();
	void object_target(Player *p);
	void toggle_objects();
	void queue_enable(int id);
	void queue_disable(int id);

	void gotEvent(BotEvent &event);

	void tell(BotEvent event);

	bool validate(CALL_HANDLE given) { return given == handle; }

	void sendPrivate(const Player *player, char *msg);
	void sendPrivate(const Player *player, BYTE snd, char *msg);

	void sendTeam(char *msg);
	void sendTeam(BYTE snd, char *msg);

	void sendTeamPrivate(Uint16 team, char *msg);
	void sendTeamPrivate(Uint16 team, BYTE snd, char *msg);

	void sendPublic(char *msg);
	void sendPublic(BYTE snd, char *msg);

	void sendPublicMacro(char *msg);
	void sendPublicMacro(BYTE snd, char *msg);

	void sendChannel(char *msg);			// #;Message
	void sendRemotePrivate(char *msg);		// :Name:Messsage
	void sendRemotePrivate(char *name, char *msg);

	void gotHelp(Player *p, Command *c);
	void gotCommand(Player *p, Command *c);
	void gotRemoteHelp(char *p, Command *c, Operator_Level l);
	void gotRemote(char *p, Command *c, Operator_Level l);
};

// The botlist contains every bot to ever be spawned of this type,
// some entries may no longer exist.

extern _linkedlist <botInfo> botlist;

botInfo *findBot(CALL_HANDLE handle);


#endif	// SPAWN_H
