#include <chrono>
#include <deque>
#include <unordered_map>
#include <list>

#include <boost/geometry/geometries/point_xy.hpp>
using namespace boost::geometry;

namespace Powerball {
	enum class Team { Warbirds, Javelins };

	class Participant {
	public:
		/* Pressure: A kill on the ball carrier, or if the ball is uncarried, a kill on the last ball carrier
		within a half second of them last possessing the ball */
		static enum class Stat { Goal, Assist, Save, GoalAllowed, BallCarry, Steal, Turnover, Kill, Death, Pressure };
		const static std::list<Stat> STATS; // List of stats for iterating

		Participant(std::string name, Team team);

		std::string getName();
		Team getTeam();
		std::chrono::seconds getPlaytime();
		std::list<std::pair<std::chrono::time_point<std::chrono::system_clock>,
			std::chrono::time_point<std::chrono::system_clock >> > getActiveTimes();

		bool isActive();
		void setActive();
		void setInactive();

		void logStat(Stat stat);
		int getStat(Stat stat);

	private:
		std::string name;
		Team team;
		bool active;
		std::chrono::time_point<std::chrono::system_clock> lastActivatedAt;

		std::list<std::pair<std::chrono::time_point<std::chrono::system_clock>,
			std::chrono::time_point<std::chrono::system_clock >> > activeTimes; // Parts of the game active
		std::unordered_map<Stat, int> stats;
	};

	struct Goal {
		Team team;
		std::string scorer;
		std::string assister;
		std::list<std::string> goalies;
		std::chrono::time_point<std::chrono::system_clock> shotAt;		// When was the ball shot
		std::chrono::time_point<std::chrono::system_clock> scoredAt;	// When the ball was scored
		std::vector<model::d2::point_xy<double>> trajectory;
		std::chrono::milliseconds carriedFor; // how many milliseconds the ballcarrid carried the ball before he shot it
	};

	class Game {
	public:
		static enum class State { NotStarted, InProgress, Finished };
		Game(std::chrono::minutes length);

		// commands to control the game
		void start();
		void end();

		// functions tracking player activity in games
		void logPilotEntry(std::string name, Team team);
		void logPilotExit(std::string name);
		void logPilotTeamSwitch(std::string name, Team newTeam);

		// functions used to track player stats in games
		void logGoal(Team team, std::list<std::string> goalies);
		void logKill(std::string killer, std::string killed);
		void logBallUpdate(std::string carrier, bool isInOwnGoal, double x, double y, double vx, double vy);

		State													getState();
		std::unordered_map<Team, int>							getScore();
		std::chrono::time_point<std::chrono::system_clock>		getTimeStarted();
		std::chrono::minutes									getLength(); // Returns the total length of the game (counting overtime)
		std::chrono::seconds									getTimePassed(); // Returns the time passed since the start of the game
		std::chrono::seconds									getPlayerPlaytime(std::string name, Team team);
		std::list<std::string>									getResultsOutput(); // Returns a list of formatted lines giving game stats
		std::list<Goal>											getGoals();
		std::list<Participant>									getParticipants();

	private:
		class BallTracker {
		public:
			struct UpdateInfo {
				bool wasCaughtOrPickedUp;	// Whether the ball was caught or picked up
				bool changedCarrier;		// Whether the ball is carried by a different carrier (could be no carrier)
				bool changedPossession;		// Whether the ball changed possession from one team to another
			};

			BallTracker(Game* game);
			UpdateInfo update(std::string carrier, double x, double y, double vx, double vy);

			bool isCarried();
			std::chrono::time_point<std::chrono::system_clock>	getLastCaughtTime();
			std::chrono::time_point<std::chrono::system_clock>	getLastCarriedTime();
			std::vector<model::d2::point_xy<double>>			getLastTrajectory();
			// offset = 0 means last, 1 means second-to-last, etc
			Participant*										getCarrier(int offset = 0);
		private:
			Game* game;

			bool carried; // Whether the ball is currently carried by a pilot (true if carried, false if it is in open space)
			std::chrono::time_point<std::chrono::system_clock> lastCarriedAt;
			std::chrono::time_point<std::chrono::system_clock> pickedUpAt;
			std::deque<Participant*> carriers; // ordered list (history) of carriers (by most recent)

			std::list<model::d2::point_xy<double>> positions; // ordered list (history) of ball position 

			double lastDirection; // direction in radians;
			model::d2::point_xy<double> lastVelocity;
			model::d2::point_xy<double> lastPosition;
			model::d2::point_xy<double> lastCarryPosition; // last position where it went from carried to uncarried
		};

		State state;
		std::unordered_map<Team, int> score;
		std::list<Goal> goals;
		std::chrono::time_point<std::chrono::system_clock> timeStarted;
		std::chrono::seconds length;
		std::list<Participant> participants; // all players who have participated
		// Maps name to participant array (2 possible participants, 1 for each team, to a single pilot name)
		std::unordered_map<std::string, std::unordered_map<Team, Participant*>> participantMap;
		BallTracker ball;

		Participant* getParticipant(std::string name, Team team);

		// Returns the participant for name that is currently active in the game.
		// (this is valid because a pilot may only be active on one team at a time)
		Participant* getActiveParticipant(std::string name);
	};
}