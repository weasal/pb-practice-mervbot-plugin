#include <string>
#include <array>
#include <unordered_map>
#include <memory>
#include <functional>
#include "gamereport.h"

class PowerballSite {
private:
	static enum class SiteAction { Verify, PilotInfo, ReportGame, BalanceTeams };
	const static std::unordered_map<SiteAction, std::string> ACTION_URIS;

	class HiddenMembers;
	std::unique_ptr<HiddenMembers> members;

	std::string getURL(SiteAction action);

	void sendPostRequest(std::string url, std::string data, std::function<void(bool, int, std::string)> callback);
	void sendGetRequest(std::string url, std::function<void(bool, int, std::string)> callback);
public:
	PowerballSite();
	~PowerballSite();
	bool verifyPilot(std::string name, std::string confirmationCode);

	bool pilotIsRegistered(std::string name);
	std::string getPilotSquad(std::string name);

	int reportGame(GameReport game, std::string mapName, bool isMatch = false, int matchId = 0); // Returns the game-id if created

	// Returns an array of vectors of two possible teams
	std::array<std::vector<std::string>, 2> getBalancedTeams(std::vector<std::string> players);
};