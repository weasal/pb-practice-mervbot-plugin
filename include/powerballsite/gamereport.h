#pragma once

#include <chrono>
#include <utility>
#include <vector>

struct LoggedGoal {
	std::string team;
	std::string scorer;
	std::string assister;
	std::chrono::time_point<std::chrono::system_clock> shotAt;
	std::chrono::time_point<std::chrono::system_clock> scoredAt;
	std::vector<std::string> scoredOn;
	std::chrono::milliseconds heldFor; // how long the ball was held for
	std::vector<std::pair<double, double>> trajectory;
};

struct LoggedParticipant {
	std::string name;
	std::string team;
	std::vector<std::pair<std::chrono::time_point<std::chrono::system_clock>,
		std::chrono::time_point<std::chrono::system_clock >> > activeTimes;
	int playtime; // seconds

	struct {
		unsigned int goals;
		unsigned int assists;
		unsigned int goalsallowed;
		unsigned int ballcarries;
		unsigned int steals;
		unsigned int saves;
		unsigned int turnovers;
		unsigned int pressures;
		unsigned int kills;
		unsigned int deaths;
	} stats;
};

struct GameReport {
	int matchId;
	std::chrono::time_point<std::chrono::system_clock> startedAt;
	struct {
		int warbirds;
		int javelins;
	} score;
	int length;

	std::vector<LoggedGoal> goals;
	std::vector<LoggedParticipant> participants;
};