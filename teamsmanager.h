#include <list>

class botInfo;

class TeamsManager {
public:
	TeamsManager(botInfo* bot, int maxTeamSize);
	int getTeamSize(int ship);
	int getMaxTeamSize();
	bool teamsAreFull(); 

	std::list<std::string> getQueue();
	/* Queues the player. Returns true if the player has successfully been queued, or false otherwise. */
	bool TeamsManager::queuePlayer(const Player* p);
	/* Remove a player from the queue, return true if removed, false otherwise. */
	void unqueuePlayer(const Player* p);

	bool playerCanEnter(const Player* p);
	void handlePlayerEntry(const Player* p, int ship);
	void handleTeamSwitch(const Player* p, int ship);
	void handlePlayerExit(const Player* p, int ship); // Ship is the old ship of the player

	void stageQueuedPlayers();

	std::list<const Player*> getActivePlayers();
	bool isActivePlayer(const Player* p);
	bool hasStagingPlayers();

private:
	botInfo* bot; 

	std::unordered_map<int, int> teamSize;
	int maxTeamSize;

	std::list<std::string> playerQueue;
	std::list<std::string> stagingPlayers;
	std::list<const Player*> activePlayers;

	bool canDequeueToStaging();
	bool isStagingPlayer(std::string name);
	void removeFromStaging(std::string name);

	// Checks if the teams are uneven, if so specs enought players the larger team
	// and puts them back at the front of the queue
	void ensureEvenTeams();

};