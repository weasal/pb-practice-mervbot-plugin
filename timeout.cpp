#include "stdafx.h"

std::list<std::unique_ptr<Timeout>> Timeout::timeouts;

Timeout::Timeout(int seconds, std::function<void(void)> callback) {
	triggered = false;
	timeoutCallback = callback;

	triggerTime = std::chrono::steady_clock::now() + std::chrono::seconds(seconds);
}

bool Timeout::checkTriggerState() {
	if (triggered)
		return true;
	
	// Trigger if current time is past trigger time.
	if (triggerTime <= std::chrono::steady_clock::now()) {
		trigger();
		return true;
	}

	return false;
}

void Timeout::trigger() {
	triggered = true;
	timeoutCallback();
}