#include "stdafx.h"
#include "spawn.h"
#include "messages.h"
#include "commtypes.cpp" // For Mervbot Command::check function
#include "algorithms.cpp" // For utility functions used in Mervbot Command class

void botInfo::gotHelp(Player *p, Command *c)
{
	// List commands
	if (!*c->final)
	{
		switch (p->access)
		{
		case OP_Owner:
		case OP_SuperModerator:
		case OP_Moderator:
			sendPrivate(p, MSG_PLUGIN_INFO);
			break;
		case OP_Limited:
		case OP_Player:
			sendPrivate(p, MSG_ABOUT_BOT);
			sendPrivate(p, MSG_BOT_HELP);
			break;
		}

		return;
	}

	// Specific command help
	switch (p->access)
	{
	case OP_Owner:
	case OP_SysOp:
	case OP_SuperModerator:
	case OP_Moderator:
	case OP_Limited:
	case OP_Player:
		break;
	}
}

void botInfo::gotCommand(Player *p, Command *c)
{
	if (!p) return;
	if (!c) return;

	switch (p->access)
	{
	case OP_Owner:
	case OP_SysOp:
	case OP_SuperModerator:
	case OP_Moderator:
	case OP_Limited:
	case OP_Player:
	default:
		if (c->check("confirm")) {
			std::string confirmationCode = c->final;
			if (site.verifyPilot(getPlayerName(p), confirmationCode)) {
				sendPrivate(p, MSG_CONFIRMSUCCESS);
			}
			else {
				sendPrivate(p, MSG_CONFIRMFAILURE);
			}
		}

		/* Queue Commands */
		if (c->check("showline")) { // showline command is consistent with 4v4 line command
			std::list<std::string> queue = teamsManager.getQueue();

			if (queue.empty()) {
				sendPrivate(p, "No pilots in line to play.");
			}
			else {
				sendPrivate(p, "Pilots in line to play: ");

				std::stringstream queuedResponse;
				bool firstEntry = true;
				for (auto iter = queue.begin(); iter != queue.end(); iter++) {
					if (!firstEntry) {
						queuedResponse << ", ";
					}
					queuedResponse << *iter;
					firstEntry = false;
				}
				queuedResponse << ".";
				sendPrivate(p, const_cast<char *>(queuedResponse.str().c_str()));
			}
		}

		if (c->check("next")) {
			teamsManager.queuePlayer(p);

			// Process the player queue (allow players to enter play if they can)
			if (botState != BotState::NoGame && botState != BotState::Intermission && botState != BotState::BalancingTeams)
				teamsManager.stageQueuedPlayers();
		}

		if (c->check("remove")) {
			teamsManager.unqueuePlayer(p);
		}

		if (c->check("sub") && teamsManager.playerCanEnter(p)) {
			int ship = SHIP_Spectator;

			if (teamsManager.getTeamSize(SHIP_Javelin) < teamsManager.getTeamSize(SHIP_Warbird))
				ship = SHIP_Javelin; // Javelins
			else if (teamsManager.getTeamSize(SHIP_Warbird) < teamsManager.getTeamSize(SHIP_Javelin))
				ship = SHIP_Warbird; // Warbirds
			else
			{
				// Teams are even
				if (currentGame) {
					std::string playerName = getPlayerName(p);
					std::chrono::seconds zeroSeconds(0);
					bool wasOnBirds = (currentGame->getPlayerPlaytime(playerName, Powerball::Team::Warbirds) != zeroSeconds);
					bool wasOnJavs = (currentGame->getPlayerPlaytime(playerName, Powerball::Team::Javelins) != zeroSeconds);
					
					// If was on a single team prior to this, make sure player subs in back on that team
					if (wasOnBirds != wasOnJavs) { // != is equivalent to exclusive OR here
						// Was on a prior team
						ship = wasOnBirds ? SHIP_Warbird : SHIP_Javelin;
					}
				}

				// This is run when the game hasn't started, or when a game has started and the player is new
				// to the game or has already played for both teams
				if (ship == SHIP_Spectator) {
					// Was either on both teams or none at all prior to this entry, so give a random team
					std::default_random_engine e;
					std::uniform_int_distribution<> distribution{ SHIP_Warbird, SHIP_Javelin };
					ship = distribution(e); // Random
				}
			}

			// Note (more info in EVENT_PlayerShip), there is no guarantee that messages
			// sent from mervBot are received in the order they were sent on clients.
			switch (ship) {
			case SHIP_Warbird:
				sendPrivate(p, CMD_SETSHIP(1));
				sendPrivate(p, CMD_SETFREQ(FREQ_WARBIRDS));
				break;
			case SHIP_Javelin:
			default:
				sendPrivate(p, CMD_SETSHIP(2));
				sendPrivate(p, CMD_SETFREQ(FREQ_JAVELINS));
				break;
			}
		}

		break;
	}
}

void botInfo::gotRemoteHelp(char *p, Command *c, Operator_Level l)
{
	// List commands
	if (!*c->final)
	{
		switch (l)
		{
		case OP_Owner:
		case OP_SysOp:
		case OP_SuperModerator:
		case OP_Moderator:
		case OP_Limited:
		case OP_Player:
			break;
		}

		return;
	}

	// Specific command help
	switch (l)
	{
	case OP_Owner:
	case OP_SysOp:
	case OP_SuperModerator:
	case OP_Moderator:
	case OP_Limited:
	case OP_Player:
		break;
	}
}

void botInfo::gotRemote(char *p, Command *c, Operator_Level l)
{
	if (!c) return;

	switch (l)
	{
	case OP_Owner:
	case OP_SysOp:
	case OP_SuperModerator:
	case OP_Moderator:
	case OP_Limited:
	case OP_Player:
		break;
	}
}
