class GoalArea {
private:
	int left;
	int top;
	int bottom;
	int right;

	Powerball::Team team;
public:
	GoalArea(Powerball::Team team, int left, int top, int right, int bottom);
	bool contains(int x, int y);
};