#include "stdafx.h"
#include "spawn.h"

GoalArea::GoalArea(Powerball::Team team, int left, int top, int right, int bottom) {
	this->team = team;
	this->left = left;
	this->top = top;
	this->right = right;
	this->bottom = bottom;
}

bool GoalArea::contains(int x, int y) {
	return (x >= left && x <= right && y >= top && y <= bottom);
}