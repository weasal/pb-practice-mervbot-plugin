#define LVZ_GAME_OVER			1
#define LVZ_JAVELINS_WIN		2
#define LVZ_WARBIRDS_WIN		3

#define LVZ_READY				4
#define LVZ_SET					5
#define LVZ_GO					6

/* Timer Lvz Ids are a number with format 3XY:
	The first digit is always 3 (the ids are all three hundred and something).
	The next digit corresponds to the position in the time, with 3 being the 4th digit (tens of minutes),
		and 0 being the last (seconds).
	The last digit is the value at the position, 0 thru 9.
	e.g. 327 will be equivalent to setting the minutes on the time panel to 7 (x7:xx). */
#define LVZ_CLOCK_BASE			300
#define LVZ_CLOCK_MIN_TENS(x)	(LVZ_CLOCK_BASE + 30 + x)
#define LVZ_CLOCK_MIN_ONES(x)	(LVZ_CLOCK_BASE + 20 + x)
#define LVZ_CLOCK_SEC_TENS(x)	(LVZ_CLOCK_BASE + 10 + x)
#define LVZ_CLOCK_SEC_ONES(x)	(LVZ_CLOCK_BASE + x)

/* Scoreboard Lvz Ids are a number with format ABC:
	The first digit corresponds to the team, either 1 (warbird) or 2 (javelins).
	The next digit corresponds to the digit (a value of 1 means the tens digit, 0 means the ones).
	The last digit is the value at the position, 0 thru 9.
	e.g. 118 will be equivalent to setting the tens digit of the warbirds score to 8 (eighty-something points!). */
#define LVZ_WARBIRD_SCORE_BASE			100
#define LVZ_WARBIRD_SCORE_TENS(x)		(LVZ_WARBIRD_SCORE_BASE + 10 + x)
#define LVZ_WARBIRD_SCORE_ONES(x)		(LVZ_WARBIRD_SCORE_BASE + x)
#define LVZ_JAVELIN_SCORE_BASE			200
#define LVZ_JAVELIN_SCORE_TENS(x)		(LVZ_JAVELIN_SCORE_BASE + 10 + x)
#define LVZ_JAVELIN_SCORE_ONES(x)		(LVZ_JAVELIN_SCORE_BASE + x)

#define LVZ_BALL_WARBIRDS				500
#define LVZ_BALL_JAVELINS				501
#define LVZ_LEAD_WARBIRDS				510
#define LVZ_LEAD_JAVELINS				511
// Game point indicator (also indicator for team that's winning in the last minute for timed games)
#define LVZ_GAMEPOINT_WARBIRDS			520
#define LVZ_GAMEPOINT_JAVELINS			521