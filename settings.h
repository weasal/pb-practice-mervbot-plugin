/* For the powerball website, current available map names: 'pb', 'pbMini' */
#define PB_SETTINGS

/* Team definitions and settings */
#define FREQ_WARBIRDS			4444
#define FREQ_JAVELINS			4445
#define DEFAULT_TEAMSIZE		7

/* Teams manager settings */
// The maximum amount of player that can queue
#define QUEUE_LIMIT				50
// The amount of time a player has to enter when it is their turn to play
#define STAGING_TIME_LIMIT		15

// The amount of time left in a game when the score board lead indicator will be switched from
// the static lvz to the flashing lvz (seconds)
#define DIRE_LEAD_TIME			30

// Amount of time between successive games in seconds
#define INTERMISSION_LENGTH		20

// End the game if a team is winning by more than this much (0 - no mercy rule)
#define MERCY_RULE_SCORE_DIFFERENCE	5

#ifdef PB_SETTINGS
	#define MAP_NAME				"pb"

	/* Powerball game settings */
	// Minimum amount of players needed for a new powerball game to start
	#define MIN_PLAYERS_TO_START	6

	// In minutes (set OVERTIME_LENGTH to 0 for no overtime)
	#define DEFAULT_GAME_LENGTH		20
	#define OVERTIME_LENGTH			5

	#define WARBIRDS_GOAL_LEFT		316 * 16
	#define WARBIRDS_GOAL_TOP		504 * 16
	#define WARBIRDS_GOAL_RIGHT		336 * 16
	#define WARBIRDS_GOAL_BOTTOM	520 * 16

	#define JAVELINS_GOAL_LEFT		688 * 16
	#define JAVELINS_GOAL_TOP		504 * 16
	#define JAVELINS_GOAL_RIGHT		708 * 16
	#define JAVELINS_GOAL_BOTTOM	520 * 16

	// The spot on map (tiles) that players from each team will be warped to before game start
	#define WARBIRDS_START_POSITION_X 325
	#define WARBIRDS_START_POSITION_Y 512
	#define JAVELINS_START_POSITION_X 697
	#define JAVELINS_START_POSITION_Y 512
	
// Amount of room players can drift from the starting position before the game starts (in tiles)
	#define START_MOVE_LIMIT 15
#endif

#ifdef PBMINI_SETTINGS
	#define MAP_NAME				"mini"

	/* Powerball game settings */
	// Minimum amount of players needed for a new powerball game to start
	#define MIN_PLAYERS_TO_START	2

	// In minutes (set OVERTIME_LENGTH to 0 for no overtime)
	#define DEFAULT_GAME_LENGTH		10
	#define OVERTIME_LENGTH			5

	#define WARBIRDS_GOAL_LEFT		401 * 16
	#define WARBIRDS_GOAL_TOP		504 * 16
	#define WARBIRDS_GOAL_RIGHT		421 * 16
	#define WARBIRDS_GOAL_BOTTOM	520 * 16

	#define JAVELINS_GOAL_LEFT		603 * 16
	#define JAVELINS_GOAL_TOP		504 * 16
	#define JAVELINS_GOAL_RIGHT		623 * 16
	#define JAVELINS_GOAL_BOTTOM	520 * 16

	// The spot on map (tiles) that players from each team will be warped to before game start
	#define WARBIRDS_START_POSITION_X 442
	#define WARBIRDS_START_POSITION_Y 512
	#define JAVELINS_START_POSITION_X 582
	#define JAVELINS_START_POSITION_Y 512

	// Amount of room players can drift from the starting position before the game starts (in tiles)
	#define START_MOVE_LIMIT 10
#endif