// This is not asynchronous, just an easy way to schedule stuff to be executed in EVENT_Tick after
// the appropriate amount of time.
class Timeout {
public:
	static std::list<std::unique_ptr<Timeout>> timeouts;

	// called in EVENT_Periodic
	static void checkAll() {
		auto it = std::remove_if(timeouts.begin(), timeouts.end(), [](std::unique_ptr<Timeout>& timeout) {
			return timeout->checkTriggerState();
		});
		timeouts.erase(it, timeouts.end());
	}

	static void set(int seconds, std::function<void (void)> callback) {
		timeouts.push_back(std::unique_ptr<Timeout>(new Timeout(seconds, callback)));
	}

	bool checkTriggerState();
private:
	Timeout(int seconds, std::function<void (void)> callback);

	//void (*timeoutCallback)();
	std::function<void ()> timeoutCallback;
	std::chrono::time_point<std::chrono::system_clock> triggerTime;
	bool triggered;

	void trigger();
};