#include "stdafx.h"	
#include "spawn.h"
#include "messages.h"
#include "lvz.h"
#include "sounds.h"
#include "dllcore.cpp"
#include "datatypes.cpp" // For Mervbot String class

// This is the message received by the bot when a goal is scored 
// (This is used to detect goals so don't change) "Enemy Goal! by "
#define GOAL_TEXT				"Enemy Goal! by "
// subtract one because sizeof includes \0 (null terminator character)
#define GOAL_TEXT_SCORER_POS	sizeof(GOAL_TEXT) - 1

// This is the (arena) message received by the bot when the arena is unlocked
#define UNLOCK_TEXT				"Arena UNLOCKED"

// This is the value used by mervbot (in the PBall struct) to indicate the ball is uncarried
#define NO_CARRIER				65535

//////// Bot list ////////

_linkedlist <botInfo> botlist;

botInfo *findBot(CALL_HANDLE handle)
{
	_listnode <botInfo> *parse = botlist.head;

	while (parse)
	{
		botInfo *item = parse->item;

		if (item->validate(handle))
			return item;

		parse = parse->next;
	}

	return 0;
}


//////// DLL "import" ////////

void botInfo::tell(BotEvent event)
{
	if (callback && handle)
	{
		event.handle = handle;
		callback(event);
	}
}

void botInfo::gotEvent(BotEvent &event)
{
	if (CONNECTION_DENIED) return;

	switch (event.code)
	{
		//////// Periodic ////////
	case EVENT_Tick:
	{
		Timeout::checkAll();

		if (me) { // Ensure in arena
			switch (botState) {
			case BotState::NoGame:
				setGametimeLvz(std::chrono::seconds(0));

				// Immediately enter intermission state and remove the ball from the arena
				botState = BotState::Intermission;
				Timeout::set(INTERMISSION_LENGTH, [this]() {
					this->botState = BotState::PlayerEntry;
				});

				sendPublic(CMD_REMOVEBALL);
				arenaHasBall = false;
				debugOut("Removing ball");
				break;
			case BotState::Intermission:
				break;
			case BotState::PlayerEntry:
			{
				teamsManager.stageQueuedPlayers();

				bool minimumMet = teamsManager.getActivePlayers().size() >= MIN_PLAYERS_TO_START;
				bool queueExhausted = teamsManager.getQueue().size() <= 1;
				bool stagingPlayers = teamsManager.hasStagingPlayers();

				if (!stagingPlayers && (teamsManager.teamsAreFull() || (minimumMet && queueExhausted))) {
					balanceTeams(); // Sent into BotState::BalancingTeams
				}
			}
				break;
			case BotState::BalancingTeams:
				if (playerSwitches.empty()) { // If bot has received events for all player switches
					teamsManager.stageQueuedPlayers();
					sendPublic(CMD_ADDBALL);
					botState = BotState::SpawningBall;
				} else {
#ifdef _DEBUG
					std::stringstream balanceLog;
					bool firstEntry = true;
					balanceLog << "Balancing: waiting for [";
					for (const Player* switchedPlayer : playerSwitches) {
						if (!firstEntry)
							balanceLog << ", ";

						balanceLog << getPlayerName(switchedPlayer);
						firstEntry = false;
					}
					balanceLog << "]";
					debugOut("%s", balanceLog.str().c_str());
#endif
				}
				break;
			case BotState::SpawningBall:
				if (arenaHasBall) {
					startPowerballGame(); // Sent into BotState::StartingGame
				}
				break;
			case BotState::StartingGame:
				// startPowerballGame sets timeouts where the state transitions to InGame automatically
				break;
			case BotState::InGame: // Fall thru
			case BotState::GameOvertime:
				// Set the time panel lvz graphics
				if (currentGame) {
					std::chrono::seconds remainingTime = currentGame->getLength() - currentGame->getTimePassed();
					if (botState == BotState::GameOvertime)
						remainingTime += std::chrono::minutes(OVERTIME_LENGTH);

					setGametimeLvz(remainingTime);

					// Change the lead indicators when it hits the game point time 
					if (remainingTime == std::chrono::seconds(DIRE_LEAD_TIME)) {
						setLeadLvz(remainingTime, currentGame->getScore());
					}
				}

				break;
			}
		}
	}
		break;
		//////// Arena ////////
	case EVENT_ArenaEnter:
	{
		arena = (char*)event.p[0];
		me = (Player*)event.p[1];	// if(me) {/*we are in the arena*/}

		rebuildPlayerMap();
		sendPublic(CMD_LOCK);
		sendPublic(CMD_SPECALL);
	}
		break;
	case EVENT_ArenaLeave:
	{
		me = 0;
	}
		break;
		//////// Soccer ////////
	case EVENT_SoccerGoal:
	{
		int freq = *(int*)&event.p[0];
		int reward = *(int*)&event.p[1];

		// This event is flawed because merv doesn't receive a goal packet (type 0x0B)
		// when arena config setting Soccer:Reward is set to 0

		// To handle that, we fire off a goal event when the bot receives an arena message
		// containing "Enemy Goal!" instead.
	}
		break;
	case EVENT_BallMove:
	{
		PBall *ball = (PBall*)event.p[0];
		if (!arenaHasBall)
			debugOut("Found ball");

		arenaHasBall = true;

		// Note, this relies on a custom mervbot implementation that correctly extrapolates
		// the ball position from the intermittent ball update packets it receieves from the server
		
		/* This code tracks who the current and last ball carriers were and their teams. If the game is in progress,
		it also logs stats for the players. */
		if (currentGame) {
			Player* carrier = nullptr;
			std::string carrierName;
			bool isInOwnGoal = false; 
			if (ball->carrier != NO_CARRIER) {
				carrier = playerMap[ball->carrier];
				carrierName = getPlayerName(carrier);
			}

			if (!previousCarrier && carrier) { // Ball picked up
				int ship = carrier->ship;
				// Update ball posession lvz graphic
				if (ship == SHIP_Warbird)
					queue_enable(LVZ_BALL_WARBIRDS);
				else
					queue_enable(LVZ_BALL_JAVELINS);

				GoalArea* ownGoal = (carrier->ship == SHIP_Warbird) ? &warbirdsGoal : &javelinsGoal;
				isInOwnGoal = ownGoal->contains(carrier->pos.x, carrier->pos.y);
			}
			else if (previousCarrier && !carrier) { // Ball shot/dropped
				queue_disable(LVZ_BALL_JAVELINS);
				queue_disable(LVZ_BALL_WARBIRDS);
			}

			toggle_objects();

			/* Apply correct scale to values (ball position is in fractional pixels * 1000, so an x position of 
			   8025.580 is stored in ball->x as 8025580, and ball velocity is in pixels per 10 seconds, so to get
			   pixels per tick (10 ms), that also needs to be divided by 1000)
			*/
			if (currentGame->getState() == Powerball::Game::State::InProgress)
				currentGame->logBallUpdate(carrierName, isInOwnGoal, ball->x / 1000.f, ball->y / 1000.f, ball->xvel / 1000.f, ball->yvel / 1000.f);

			previousCarrier = carrier;
		}
	}
		break;
		//////// Player ////////
	case EVENT_PlayerEntering:
	{
		Player *p = (Player*)event.p[0];
		playerMap[p->ident] = p;

		if (currentGame) {
			setGamescoreLvz(currentGame->getScore(), p);
		}
	}
		break;
	case EVENT_PlayerMove:
	{
		Player *p = (Player*)event.p[0];

		// Warp players to start position for games not in progress
		if (p->ship != SHIP_Spectator && botState == BotState::StartingGame) {
			int distanceSquared, dx, dy;
			std::string moveCommand;
			if (p->ship == SHIP_Warbird) {
				dx = p->tile.x - WARBIRDS_START_POSITION_X;
				dy = p->tile.y - WARBIRDS_START_POSITION_Y;
				moveCommand = CMD_MOVE_TO(WARBIRDS_START_POSITION_X, WARBIRDS_START_POSITION_Y);
			}
			else {
				dx = p->tile.x - JAVELINS_START_POSITION_X;
				dy = p->tile.y - JAVELINS_START_POSITION_Y;
				moveCommand = CMD_MOVE_TO(JAVELINS_START_POSITION_X, JAVELINS_START_POSITION_Y);
			}

			distanceSquared = (dx * dx) + (dy * dy);

			if (distanceSquared > START_MOVE_LIMIT * START_MOVE_LIMIT) {
				sendPrivate(p, const_cast<char*>(moveCommand.c_str()));
				sendPrivate(p, CMD_ENGINES_SHUTDOWN);
			}
		}
	}
		break;
	case EVENT_PlayerDeath:
	{
		Player *p = (Player*)event.p[0],
			*k = (Player*)event.p[1];

		if (currentGame && currentGame->getState() == Powerball::Game::State::InProgress) {
			currentGame->logKill(getPlayerName(k), getPlayerName(p));
		}
	}
		break;
	case EVENT_PlayerShip:
	{
		Player *p = (Player*)event.p[0];
		Uint16 oldShip = (Uint16)(Uint32)event.p[1];
		Uint16 oldFreq = (Uint16)(Uint32)event.p[2];

		debugOut("Player %s entered ship: %s", getPlayerName(p).c_str(), 
			(p->ship == SHIP_Warbird) ? "birds" : "javs");

		// PowerballBot is coded to send *setship before *setfreq, however
		// sometimes the messages are received out of order on player !sub, so 
		// they receive *setfreq first and then *setship (despite them being sent in the opposite order)
		// so that would have them playing on a random private freq, which is corrected here by sending
		// an additional setfreq command
		if (p->ship == SHIP_Warbird) {
			sendPrivate(p, CMD_SETFREQ(FREQ_WARBIRDS));
		}
		else {
			sendPrivate(p, CMD_SETFREQ(FREQ_JAVELINS));
		}

		bool teamSwitch = false;
		if (teamsManager.isActivePlayer(p)) {
			teamSwitch = true;
			teamsManager.handleTeamSwitch(p, p->ship);
		}
		else {
			teamsManager.handlePlayerEntry(p, p->ship);
		}

		if (currentGame) {
			if (teamSwitch)
				currentGame->logPilotTeamSwitch(getPlayerName(p), SHIP_TO_POWERBALLTEAM(p->ship));
			else
				currentGame->logPilotEntry(getPlayerName(p), SHIP_TO_POWERBALLTEAM(p->ship));

			if (currentGame->getState() == Powerball::Game::State::InProgress)
				sendPrivate(p, CMD_ENGINES_SHUTDOWN);
		}
	}
		break;
	case EVENT_PlayerSpec:
	{
		Player *p = (Player*)event.p[0];
		Uint16 oldFreq = (Uint16)(Uint32)event.p[1];
		Uint16 oldShip = (Uint16)(Uint32)event.p[2];

		playerSpecOrQuit(p, oldShip);
	}
		break;
	case EVENT_PlayerTeam:
	{
		// Don't care about active players switching teams here (this could be spectators choosing freqs and stuff)
		// That's handled in the player ship event

		Player *switched = (Player*)event.p[0];	
		Uint16 oldteam = (Uint16)(Uint32)event.p[1];
		Uint16 oldship = (Uint16)(Uint32)event.p[2];

		/* Check if players who's teams were switched while in BotState::BalancingTeams have occured */
		if (botState == BotState::BalancingTeams) {
			debugOut("Players freq changed while balancing teams: %s", getPlayerName(switched).c_str());
			playerSwitches.remove(switched);
		}
	}
		break;
	case EVENT_PlayerLeaving:
	{
		Player *p = (Player*)event.p[0];

		playerSpecOrQuit(p, p->ship);

		playerMap.erase(p->ident);
	}
		break;
		//////// Chat ////////
	case EVENT_Chat:
	{
		int type = *(int*)&event.p[0];
		int sound = *(int*)&event.p[1];
		Player *p = (Player*)event.p[2];
		std::string message = (char*)event.p[3];

		switch (type)
		{
		case MSG_Arena:
			// See EVENT_SoccerGoal for why this is here
			if ((botState == BotState::InGame || botState == BotState::GameOvertime) &&
				message.find(GOAL_TEXT) != std::string::npos) {
				std::string scorerName = message.substr(GOAL_TEXT_SCORER_POS);

				Player* scorer = nullptr;
				_listnode <Player> *parse = playerlist->head;
				while (parse) {
					Player *p = parse->item;

					if (scorerName == getPlayerName(p)) {
						scorer = p;
						break;
					}

					parse = parse->next; // parse moved to the next link
				}

				if (scorer) {
					Powerball::Team team = SHIP_TO_POWERBALLTEAM(scorer->ship);
					int opposingTeamShip = (scorer->ship == SHIP_Warbird) ? SHIP_Javelin : SHIP_Warbird;
						
					GoalArea* defendingGoal = (team == Powerball::Team::Warbirds) ? &javelinsGoal : &warbirdsGoal;
					std::list<std::string> goalies;
					for (const Player* player : teamsManager.getActivePlayers()) {
						if (player->ship == opposingTeamShip && defendingGoal->contains(player->pos.x, player->pos.y)) {
							goalies.push_back(getPlayerName(player));
						}
					}

					if (currentGame) {
						currentGame->logGoal(team, goalies);
						std::unordered_map<Powerball::Team, int> score = currentGame->getScore();

						std::stringstream scoreOutput;
						int warbirdsScore = score[Powerball::Team::Warbirds];
						int javelinsScore = score[Powerball::Team::Javelins];

						bool tied = (warbirdsScore == javelinsScore);
						scoreOutput << CMD_ARENA_MSG "Score: " << warbirdsScore << "-" << javelinsScore << " ";

						if (tied)
							scoreOutput << MSG_TIED;
						else {
							if (warbirdsScore > javelinsScore)
								scoreOutput << MSG_WBSWINNING;
							else
								scoreOutput << MSG_JAVSWINNING;
						}

						sendPublic((team == Powerball::Team::Warbirds) ? SOUND_WARBIRDS_SCORE : SOUND_JAVELINS_SCORE,
							const_cast<char*>(scoreOutput.str().c_str()));

						setGamescoreLvz(score);
						setLeadLvz(currentGame->getLength() - currentGame->getTimePassed(), score);

						int difference = abs(warbirdsScore - javelinsScore);
						if (MERCY_RULE_SCORE_DIFFERENCE != 0 && difference > MERCY_RULE_SCORE_DIFFERENCE) {
							sendPublic(MSG_MERCY_RULE_END);
							endPowerballGame();
						}
					}

					if (botState == BotState::GameOvertime)
						endPowerballGame();
				}
			}
			else if (message.find(UNLOCK_TEXT) != std::string::npos) {
				debugOut("Arena unlocked (oops, must've already been locked), locking arena");
				sendPublic(CMD_LOCK);
			}
			break;
		case MSG_PublicMacro:		
			if (!p) break;
			break;
		case MSG_Public:			
			if (!p) break;
			break;
		case MSG_Team:				
			if (!p) break;
			break;
		case MSG_TeamPrivate:		
			if (!p) break;
			break;
		case MSG_Private:			
			if (!p) break;
			break;
		case MSG_PlayerWarning:		
			if (!p) break;
			break;
		case MSG_RemotePrivate:
			break;
		case MSG_ServerError:
			break;
		case MSG_Channel:
			break;
		};
	}
		break;
	case EVENT_LocalCommand:
	{
		Player *p = (Player*)event.p[0];
		Command *c = (Command*)event.p[1];

		gotCommand(p, c);
	}
		break;
	case EVENT_LocalHelp:
	{
		Player *p = (Player*)event.p[0];
		Command *c = (Command*)event.p[1];

		gotHelp(p, c);
	}
		break;
	case EVENT_RemoteCommand:
	{
		char *p = (char*)event.p[0];
		Command *c = (Command*)event.p[1];
		Operator_Level o = *(Operator_Level*)&event.p[2];

		gotRemote(p, c, o);
	}
		break;
	case EVENT_RemoteHelp:
	{
		char *p = (char*)event.p[0];
		Command *c = (Command*)event.p[1];
		Operator_Level o = *(Operator_Level*)&event.p[2];

		gotRemoteHelp(p, c, o);
	}
		break;
		//////// Containment ////////
	case EVENT_Init:
	{
		int major = HIWORD(*(int*)&event.p[0]);
		int minor = LOWORD(*(int*)&event.p[0]);
		callback = (CALL_COMMAND)event.p[1];
		playerlist = (CALL_PLIST)event.p[2];
		flaglist = (CALL_FLIST)event.p[3];
		map = (CALL_MAP)event.p[4];
		bricklist = (CALL_BLIST)event.p[5];

		if (major > CORE_MAJOR_VERSION)
		{
			tell(makeEcho("DLL plugin cannot connect.  This plugin is out of date."));

			CONNECTION_DENIED = true;

			return;
		}
		else if ((major < CORE_MAJOR_VERSION) || (minor < CORE_MINOR_VERSION))
		{
			tell(makeEcho("DLL plugin cannot connect.  This plugin requires the latest version of MERVBot."));

			CONNECTION_DENIED = true;

			return;
		}
		else
		{
			tell(makeEcho("DLL plugin connected."));
		}

		rebuildPlayerMap();
	}
		break;
	case EVENT_Term:
	{
#ifdef _DEBUG
		_CrtDumpMemoryLeaks();
#endif
		tell(makeEcho("DLL plugin disconnected."));
	}
		break;
	};
}

// Default mervbot botInfo functions
//////// DLL export ////////

_declspec(dllexport) void __stdcall talk(BotEvent &event)
{
	botInfo *bot;

	bot = findBot(event.handle);

	if (!bot)
	{
		bot = new botInfo(event.handle);
		botlist.append(bot);
	}

	if (bot) bot->gotEvent(event);
}

//////// LVZ Object Toggling ////////

void botInfo::clear_objects()
{
	num_objects = 0;
}

void botInfo::object_target(Player *p)
{
	object_dest = p;
}

void botInfo::toggle_objects()
{
	Player *p = object_dest;

	if (!p)	tell(makeToggleObjects(UNASSIGNED, (Uint16 *)object_array, num_objects));
	else	tell(makeToggleObjects(p->ident, (Uint16 *)object_array, num_objects));

	num_objects = 0;
}

void botInfo::queue_enable(int id)
{
	if (num_objects == MAX_OBJECTS)
		toggle_objects();

	object_array[num_objects].id = id;
	object_array[num_objects].disabled = false;
	++num_objects;
}

void botInfo::queue_disable(int id)
{
	if (num_objects == MAX_OBJECTS)
		toggle_objects();

	object_array[num_objects].id = id;
	object_array[num_objects].disabled = true;
	++num_objects;
}


//////// Chatter ////////

void botInfo::sendPrivate(const Player *player, BYTE snd, char *msg)
{
	tell(makeSay(MSG_Private, snd, player->ident, msg));
}

void botInfo::sendPrivate(const Player *player, char *msg)
{
	tell(makeSay(MSG_Private, 0, player->ident, msg));
}

void botInfo::sendTeam(char *msg)
{
	tell(makeSay(MSG_Team, 0, 0, msg));
}

void botInfo::sendTeam(BYTE snd, char *msg)
{
	tell(makeSay(MSG_Team, snd, 0, msg));
}

void botInfo::sendTeamPrivate(Uint16 team, char *msg)
{
	tell(makeSay(MSG_TeamPrivate, 0, team, msg));
}

void botInfo::sendTeamPrivate(Uint16 team, BYTE snd, char *msg)
{
	tell(makeSay(MSG_TeamPrivate, snd, team, msg));
}

void botInfo::sendPublic(char *msg)
{
	tell(makeSay(MSG_Public, 0, 0, msg));
}

void botInfo::sendPublic(BYTE snd, char *msg)
{
	tell(makeSay(MSG_Public, snd, 0, msg));
}

void botInfo::sendPublicMacro(char *msg)
{
	tell(makeSay(MSG_PublicMacro, 0, 0, msg));
}

void botInfo::sendPublicMacro(BYTE snd, char *msg)
{
	tell(makeSay(MSG_PublicMacro, snd, 0, msg));
}

void botInfo::sendChannel(char *msg)
{
	tell(makeSay(MSG_Channel, 0, 0, msg));
}

void botInfo::sendRemotePrivate(char *msg)
{
	tell(makeSay(MSG_RemotePrivate, 0, 0, msg));
}

void botInfo::sendRemotePrivate(char *name, char *msg)
{
	String s;
	s += ":";
	s += name;
	s += ":";
	s += msg;

	sendRemotePrivate(s);
}
